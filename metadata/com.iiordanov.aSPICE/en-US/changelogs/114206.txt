v4.1.0
- Option to disable cursor
- Fixes to cursor shape handling
- Fix for devices with cutouts
- Fix for back button right click
- Fix for pointer bad pointer location when pointer shape changes
v4.0.9
- Japanese, Korean, Traditional Chinese, and Simplified Chinese localizations
v4.0.8
- Security updates
v4.0.7
- 64-bit support
- Bugfixes
v4.0.6
- Support for SPICE server mouse mode
- Bugfix to sshlib for sha2-256 and sha2-512 keys
v4.0.5
- Bugfixes
- Logcat copy from previous runs as well
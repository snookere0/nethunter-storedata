Conveniently manage and share your Internet connection with the WiFi Pineapple.

* Share your Android Internet connection with the WiFi Pineapple via USB Tethering (without root)
* Automatically establish a secure connection to the WiFi Pineapple web interface.
* Setup wizard detects new WiFi Pineapples and guides you through initial configuration.

Supports 6th generation WiFi Pineapple devices from Hak5.

If you're having problems with the software, please contact support@hak5.org

*Carrier tethering charges may apply
 
